class ChangeDatatypeInUser < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :age, :integer, using: 'age::integer'
    rename_column :users, :fixed_country, :country
  end
end
