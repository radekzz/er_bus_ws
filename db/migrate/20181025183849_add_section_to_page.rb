class AddSectionToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :section, :string
  end
end
