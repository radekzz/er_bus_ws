require 'rails_helper'

RSpec.describe 'POST /signup', type: :request do
  subject(:signup) { post url, params: params }
  let(:url) { '/api/v1/users' }
  let(:user_attributes) { attributes_for(:user) }
  let(:params) { { user: user_attributes } }

  context 'when user is unauthenticated' do
    it 'returns 201' do
      signup
      expect(response.status).to eq 201
    end

    it 'returns a new user' do
      signup
      expect(json_body['email']).to eq(user_attributes[:email])
    end
  end

  context 'when user already exists' do
    before { create(:user, user_attributes) }

    it 'returns bad request status' do
      signup
      expect(response.status).to eq 422
    end

    it 'returns validation errors' do
      signup
      expect(json_body['errors']['email'].first).to eq('has already been taken')
    end
  end
end
