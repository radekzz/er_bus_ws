RSpec.describe 'DELETE /logout', type: :request do
  let(:url) { '/api/v1/users/sign_out' }

  it 'returns 204, no content' do
    delete url
    expect(response).to have_http_status(204)
  end
end
