FactoryBot.define do
  factory :page do
    content { Faker::Lorem.paragraph }
    name { Faker::Lorem.word }
    section { Faker::Lorem.word }
  end
end
