require 'rails_helper'

RSpec.describe User, type: :model do
  describe '.valid?' do
    subject(:user) { described_class.new(user_attributes) }
    let(:user_attributes) { attributes_for(:user) }

    context 'When name and surname are given' do
      let(:user_attributes) { attributes_for(:user) }

      it 'returns true' do
        expect(user.valid?).to be_truthy
      end
    end

    context 'When name and surname are nil' do
      let(:user_attributes) { attributes_for(:user, name: nil, surname: nil) }

      it 'returns false' do
        expect(user.valid?).to be_falsey
      end
    end

    context 'When name and surname are empty' do
      let(:user_attributes) { attributes_for(:user, name: '', surname: '') }

      it 'returns false' do
        expect(user.valid?).to be_falsey
      end
    end
  end
end
