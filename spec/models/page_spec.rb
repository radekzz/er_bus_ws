require 'rails_helper'

RSpec.describe Page, type: :model do
  describe '.valid?' do
    subject(:page) { described_class.new(page_attributes) }
    let(:page_attributes) { attributes_for(:page) }

    it 'returns true' do
      expect(page.valid?).to be_truthy
    end

    context 'When name is nil' do
      let(:page_attributes) { attributes_for(:page, name: nil) }

      it 'returns false' do
        expect(page.valid?).to be_falsey
      end
    end

    context 'When name is empty' do
      let(:page_attributes) { attributes_for(:page, name: '') }

      it 'returns false' do
        expect(page.valid?).to be_falsey
      end
    end

    context 'When section with name are not unique' do
      let(:page_attributes) { attributes_for(:page, section: section, name: name) }
      let(:section) { 'section' }
      let(:name) { 'name' }

      before { create(:page, name: name, section: section) }

      it 'should be not valid' do
        expect(page.valid?).to be_falsey
      end
    end
  end
end
