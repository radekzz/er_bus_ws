require 'rails_helper'

RSpec.describe API::V1::Actions::Pages::Update do
  subject(:action) { described_class.new(controller: controller, pages_model: pages_model) }
  let(:controller) { double('controller') }
  let(:pages_model) { double(Page) }

  describe '.perform' do
    let(:request_params) { double('request_params') }
    let(:expected_result) { pages }
    let(:page_attributes) { attributes_for(:page) }
    let(:page_id) { 'page_id' }
    let(:page) { double('page') }
    let(:valid_page?) { true }

    before { request_params.stub_chain(:require, :permit) { page_attributes } }

    it 'renders success ' do
      expect(request_params).to receive(:[]).with(:id) { page_id }
      expect(pages_model).to receive(:find_by_id).with(page_id) { page }
      expect(page).to receive(:assign_attributes).with(page_attributes)
      expect(page).to receive(:valid?) { valid_page? }
      expect(page).to receive(:save)
      expect(page).to receive(:reload) { page }
      expect(controller).to receive(:success).with(page)

      action.perform(request_params)
    end

    context 'When page with id does not exist' do
      let(:page) { nil }

      it 'renders not_found' do
        expect(request_params).to receive(:[]).with(:id) { page_id }
        expect(pages_model).to receive(:find_by_id).with(page_id) { page }
        expect(controller).to receive(:not_found)

        action.perform(request_params)
      end
    end

    context 'When page is not valid' do
      let(:errors) { ['some_error'] }
      let(:valid_page?) { false }

      it 'renders unprocessable_entity with errors' do
        expect(request_params).to receive(:[]).with(:id) { page_id }
        expect(pages_model).to receive(:find_by_id).with(page_id) { page }
        expect(page).to receive(:assign_attributes).with(page_attributes)
        expect(page).to receive(:valid?) { valid_page? }
        expect(page).to receive(:errors) { errors }
        expect(controller).to receive(:unprocessable_entity).with(errors)

        action.perform(request_params)
      end
    end
  end
end
