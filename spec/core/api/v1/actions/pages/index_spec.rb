require 'rails_helper'

RSpec.describe API::V1::Actions::Pages::Index do
  subject(:action) { described_class.new(controller: controller, pages_model: pages_model) }
  let(:controller) { double('controller') }
  let(:pages_model) { double(Page) }

  describe '.perform' do
    let(:expected_result) { pages }
    let(:pages) { %w[pages1 pages2] }

    it 'renders success ' do
      expect(pages_model).to receive(:all) { pages }
      expect(controller).to receive(:success).with(pages)

      action.perform
    end
  end
end
