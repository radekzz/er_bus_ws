require 'rails_helper'

RSpec.describe API::V1::Actions::Pages::Show do
  subject(:action) { described_class.new(controller: controller, pages_model: pages_model) }
  let(:controller) { double('controller') }
  let(:pages_model) { double(Page) }

  describe '.perform' do
    let(:expected_result) { pages }
    let(:page) { attributes_for(:page) }
    let(:params) { { name: page[:name], section: page[:section] } }

    it 'renders success ' do
      expect(pages_model).to receive(:find_by).with(params) { page }
      expect(controller).to receive(:success).with(page)

      action.perform(params)
    end
  end
end
