module API
  module V1
    module Helper
      def json_body
        JSON.parse(response.body)
      end
    end
  end
end
