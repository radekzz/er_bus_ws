require 'rails_helper'

RSpec.describe API::V1::PagesController, type: :controller do
  describe 'GET #index' do
    subject(:get_index) { get :index, params: params }
    let(:params) { {} }
    let!(:pages) { create_list(:page, 10) }
    let(:expected_response) { JSON.parse pages.to_json }

    it 'returns http success' do
      get_index
      expect(response).to have_http_status(:success)
    end

    it 'returns pages' do
      get_index
      expect(json_body).to eq(expected_response)
    end
  end

  describe 'GET #show' do
    subject(:get_show) { get :show, params: params }
    let(:params) { { name: name, section: section } }
    let(:name) { page.name }
    let(:section) { page.section }
    let(:page) { create(:page) }
    let(:expected_response) { JSON.parse page.to_json }

    it 'returns http success' do
      get_show
      expect(response).to have_http_status(:success)
    end

    it 'returns page' do
      get_show
      expect(json_body).to eq(expected_response)
    end

    context 'When page does not exist' do
      let(:name) { 'name_does_not_exists' }
      let(:section) { 'section_does_not_exists' }

      it 'returns http not_found' do
        get_show
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST #update' do
    subject(:post_update) { post :update, params: params }
    let(:params) { { id: page[:id], page: page_attributes } }
    let(:page_attributes) { attributes_for(:page) }
    let(:page) { create(:page) }

    it 'returns http unauthorized' do
      post_update
      expect(response).to have_http_status(:unauthorized)
    end

    context 'When user is sign in' do
      let(:user) { create(:user) }
      let(:expected_response) { JSON.parse page_attributes.to_json }

      before { sign_in user }

      it 'returns http success' do
        post_update
        expect(response).to have_http_status(:success)
      end

      it 'returns page' do
        post_update
        expect(json_body).to include(expected_response)
      end

      context 'When page is invalid' do
        let(:page_attributes) { attributes_for(:page, name: nil) }

        it 'returns unprocessable entity' do
          post_update
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns errors' do
          post_update
          expect(json_body).to have_key('errors')
        end
      end
    end
  end
end
