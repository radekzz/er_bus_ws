require 'rails_helper'

RSpec.describe HealthController, type: :controller do
  describe 'GET #check' do
    subject(:get_check) { get :check }
    let(:expected_response) { { 'status' => 'ok' } }

    it 'returns status ok' do
      get_check
      expect(json_body).to eq(expected_response)
    end

    it 'returns http status ok' do
      get_check
      expect(response).to have_http_status(:ok)
    end
  end
end
