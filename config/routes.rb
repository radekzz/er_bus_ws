Rails.application.routes.draw do
  get 'health/check'
  devise_for 'users', path_prefix: 'api/v1', defaults: { format: :json }
  namespace :api do
    namespace :v1 do
      get 'main/index'

      get 'pages', to: 'pages#index'
      get 'pages/:name/:section', to: 'pages#show'
      post 'pages/:id', to: 'pages#update'
    end
  end
end
