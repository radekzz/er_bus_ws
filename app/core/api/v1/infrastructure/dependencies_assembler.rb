module API
  module V1
    module Infrastructure
      class DependenciesAssembler
        def initialize(controller)
          @controller = controller
        end

        def pages_index_action
          Actions::Pages::Index.new(
            controller: controller,
            pages_model: pages_model
          )
        end

        def pages_show_action
          Actions::Pages::Show.new(
            controller: controller,
            pages_model: pages_model
          )
        end

        def pages_update_action
          Actions::Pages::Update.new(
            controller: controller,
            pages_model: pages_model
          )
        end

        def pages_model
          ::Page
        end

        private

        attr_reader :controller
      end
    end
  end
end
