module API
  module V1
    module Actions
      module Pages
        class Index
          def initialize(dependencies)
            @controller = dependencies.fetch(:controller)
            @pages_model = dependencies.fetch(:pages_model)
          end

          def perform
            response = fetch_data
            controller.success(response)
          end

          private

          attr_reader :controller, :pages_model

          def fetch_data
            pages_model.all
          end
        end
      end
    end
  end
end
