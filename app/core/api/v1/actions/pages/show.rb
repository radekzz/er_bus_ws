module API
  module V1
    module Actions
      module Pages
        class Show
          def initialize(dependencies)
            @controller = dependencies.fetch(:controller)
            @pages_model = dependencies.fetch(:pages_model)
          end

          def perform(params)
            response = fetch_data(params)
            response.nil? ? controller.not_found : controller.success(response)
          end

          private

          attr_reader :controller, :pages_model

          def fetch_data(params)
            pages_model.find_by(
              name: params[:name],
              section: params[:section]
            )
          end
        end
      end
    end
  end
end
