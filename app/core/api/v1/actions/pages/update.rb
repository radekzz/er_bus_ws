module API
  module V1
    module Actions
      module Pages
        class Update
          PAGE_ATTRIBUTES = %i[name content section].freeze

          def initialize(dependencies)
            @controller = dependencies.fetch(:controller)
            @pages_model = dependencies.fetch(:pages_model)
          end

          def perform(params)
            page_attributes = params.require(:page).permit(PAGE_ATTRIBUTES)
            page = pages_model.find_by_id(params[:id])

            page.nil? ? controller.not_found : process_update(page, page_attributes)
          end

          private

          attr_reader :controller, :pages_model

          def process_update(page, page_attributes)
            page.assign_attributes(page_attributes)
            page.valid? ? save_entity(page) : unprocessable_entity(page)
          end

          def save_entity(page)
            page.save
            controller.success(page.reload)
          end

          def unprocessable_entity(page)
            controller.unprocessable_entity(page.errors)
          end
        end
      end
    end
  end
end
