class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  before_action :configure_permitted_parameters, if: :devise_controller?
  respond_to :json

  def success(response)
    render json: response, status: :ok
  end

  def not_found
    render nothing: true, status: :not_found
  end

  def unprocessable_entity(errors)
    render json: { errors: errors }, status: :unprocessable_entity
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name surname])
  end
end
