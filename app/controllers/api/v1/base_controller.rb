module API
  module V1
    class BaseController < ApplicationController
      def dependencies_assembler
        @dependencies_assembler ||= API::V1::Infrastructure::DependenciesAssembler.new(self)
      end
    end
  end
end
