module API
  module V1
    class PagesController < BaseController
      before_action :authenticate_user!, only: [:update]

      def show
        action = dependencies_assembler.pages_show_action
        action.perform(params)
      end

      def index
        action = dependencies_assembler.pages_index_action
        action.perform
      end

      def update
        action = dependencies_assembler.pages_update_action
        action.perform(params)
      end
    end
  end
end
