class Page < ApplicationRecord
  validates_presence_of :name
  validates :section, uniqueness: { scope: :name }
end
